# Parallelising Python

Let's use all the cores, sorta.

This will focus on multiprocessing. Multithreading also exists, but is useful for other things. Multiprocessing should allow you to run a function in parallel with multiple cores. But there is an overhead cost, and unlike multithreading, it is harder to share objects between processes.

## Introduction
Lets say we have a function that does something to a sequences. It could be any function, but for example this function takes a sequence as a string and counts the number of individual ACTGNs to give us the percentages of each base.

```python
from Bio import SeqIO 
from Bio.SeqRecord import SeqRecord
from Bio.Seq import Seq
def countATCG(seq):
    s=seq.seq
    d,n={},0
    for b in ['A','C','G','T','N']:
        d[b]=(s.count(b)/len(s))*100
    d['id'] = seq.id
    return d

seq=SeqRecord(Seq('AAACCCGGGTTTN'),id="seq1")
countATCG(seq)                                                                                                 
#{'A': 23.076923076923077,
# 'C': 23.076923076923077,
# 'G': 23.076923076923077,
# 'T': 23.076923076923077,
# 'N': 7.6923076923076925,
# 'id': 'seq1'}}
```

We can then iterate through a fasta file of sequences to get the results for each sequence

```python
import sys
import pandas as pd

results=[]
for seq in SeqIO.parse(open('cog_select.fasta', 'rt'), 'fasta'):
    d=countATCG(seq)
    results.append(d)

df=pd.DataFrame(results)
print(df)

# Output #
#              A          C          G          T          N                        id
# 0    20.634974  12.542237  13.575993  21.611856  31.634940  England/PORT-2E9AB2/2020
# 1    29.405076  18.081798  19.399391  31.702505   1.411230  England/PORT-2E9A94/2020
# 2    17.851052  11.152727  11.657693  19.309099  40.029428  England/PORT-2E9A49/2020
# 3    25.060233  15.148574  16.356579  26.639673  16.794940  England/PORT-2E9AD0/2020
# 4    28.348326  17.426345  18.650303  30.381567   5.193459  England/PORT-2E9A85/2020
# ..         ...        ...        ...        ...        ...                       ...
# 395  28.170099  17.043362  18.285619  30.025113   6.465763   England/NORW-F5305/2020
# 396  29.762741  18.301375  19.583041  32.068400   0.284443   England/NORW-F59E5/2020
# 397  29.578831  18.134680  19.369083  31.716455   1.197605  England/NORW-21F734/2020
# 398  27.550645  16.758748  17.950779  29.435794   8.297338   England/NORW-F597C/2020
# 399  29.608677  18.143474  19.415526  31.771165   1.061159   England/NORW-FA142/2020
# 
# [400 rows x 6 columns]

```

## multiprocessing
It is quite easy to run this example using "Pool" (https://docs.python.org/3/library/multiprocessing.html#module-multiprocessing). All we need is the function, a list of sequences, and the multiprocessing library. 


```python
from multiprocessing import Pool                                                                            

threads=4
with Pool(threads) as p: 
    results=p.map(countATCG, [seq for seq in SeqIO.parse(open('cog_select.fasta', 'rt'), 'fasta')] ) 
                                                                                                            
df=pd.DataFrame(results)
print(df)                                
```

For more control, you might consider the 'Process' function from 'multiprocessing.

## which is faster, iterating or multiprocessing?
I'm going to run with with this big file from COG, which is ~7gb, so will take a lot longer to run. You won't be able to run this code exactley then, unless you are also called nick and have copied it to a folder in your home directory.

I'm also importing "time" so we can compare running times for iterating or multiprocessing the sequences.

```python
import time

#sqfile='cog_select.fasta'
sqfile='/Users/nick/covid/cog_all.fasta'

def runIter():
    start = time.time()
    results=[]
    for seq in SeqIO.parse(open(sqfile, 'rt'), 'fasta'): 
        d=countATCG(seq) 
        results.append(d) 
    df=pd.DataFrame(results)
    print(df)
    end = time.time()
    print(end - start)

def runPool():
    start = time.time() 
    threads=8
    with Pool(threads) as p:
        results=p.map(countATCG,[seq for seq in SeqIO.parse(open(sqfile, 'rt'), 'fasta')])     
    df=pd.DataFrame(results)
    print(df)
    end = time.time()
    print(end - start)


# runIter()                                                                                         
#                 A          C          G          T          N                        id
# 0       20.634974  12.542237  13.575993  21.611856  31.634940  England/PORT-2E9AB2/2020
# 1       29.405076  18.081798  19.399391  31.702505   1.411230  England/PORT-2E9A94/2020
# 2       17.851052  11.152727  11.657693  19.309099  40.029428  England/PORT-2E9A49/2020
# 3       25.060233  15.148574  16.356579  26.639673  16.794940  England/PORT-2E9AD0/2020
# 4       28.348326  17.426345  18.650303  30.381567   5.193459  England/PORT-2E9A85/2020
# ...           ...        ...        ...        ...        ...                       ...
# 146411  29.191051  17.974785  19.182022  31.381467   2.270675       Scotland/CVR04/2020
# 146412  29.736147  18.292479  19.569943  31.996790   0.404642       Scotland/CVR05/2020
# 146413  29.388356  18.078454  19.322476  31.595492   1.615223       Scotland/CVR03/2020
# 146414  29.191051  17.974785  19.182022  31.384811   2.267331       Scotland/CVR02/2020
# 146415  29.492024  18.128616  19.392703  31.685784   1.300873       Scotland/CVR01/2020
# 
# [146416 rows x 6 columns]
# 44.44187688827515
# 
# runPool()                                                                                         
#                 A          C          G          T          N                        id
# 0       20.634974  12.542237  13.575993  21.611856  31.634940  England/PORT-2E9AB2/2020
# 1       29.405076  18.081798  19.399391  31.702505   1.411230  England/PORT-2E9A94/2020
# 2       17.851052  11.152727  11.657693  19.309099  40.029428  England/PORT-2E9A49/2020
# 3       25.060233  15.148574  16.356579  26.639673  16.794940  England/PORT-2E9AD0/2020
# 4       28.348326  17.426345  18.650303  30.381567   5.193459  England/PORT-2E9A85/2020
# ...           ...        ...        ...        ...        ...                       ...
# 146411  29.191051  17.974785  19.182022  31.381467   2.270675       Scotland/CVR04/2020
# 146412  29.736147  18.292479  19.569943  31.996790   0.404642       Scotland/CVR05/2020
# 146413  29.388356  18.078454  19.322476  31.595492   1.615223       Scotland/CVR03/2020
# 146414  29.191051  17.974785  19.182022  31.384811   2.267331       Scotland/CVR02/2020
# 146415  29.492024  18.128616  19.392703  31.685784   1.300873       Scotland/CVR01/2020
# 
# [146416 rows x 6 columns]
# 29.62968397140503
```

## Caveats
So in python you can parallelize your code fairly easily, but its not a magic bullet, there are overheads that can cause it be inefficient. You might sometimes be better to run the script independently on different files.

See also:
GNU Parallel	gnu.org/software/bash/manual/html_node/GNU-Parallel.html
Nextflow	https://www.nextflow.io/docs/latest/getstarted.html
Snakemake	https://snakemake.readthedocs.io/en/stable/ 



---



## Note from discussion: capturing jobs as they finish

The following example code uses a alternative API called `concurrent.futures` to fire off each function call in its own process, and print them as they return. This is an example only, and parallel or not is a very slow way to count ATCG content!

```python
import concurrent.futures

def countATCG(record):  # Nick's example function
    s=record.seq
    d,n={},0
    for b in ['A','C','G','T','N']:
        d[b]=(s.count(b)/len(s))*100
    d['id'] = record.id
    return d
  
records = SeqIO.parse(open('cog_select.fasta', 'rt'), 'fasta')

with concurrent.futures.ProcessPoolExecutor(max_workers=8) as x:
    futures = {x.submit(countATCG, record): record for record in records}
    for future in concurrent.futures.as_completed(futures):
        record = futures[future]
        print(record.id, future.result())
```
