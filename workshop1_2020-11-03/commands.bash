
~/nextflow run main.nf --infi accessions.txt \
	 -resume \
	--spBin /gpfs0/apps/well/SPAdes/3.6.0/bin/ \
	--home /users/bag/clme1715 \
	-with-trace \
	-with-report spades.html
