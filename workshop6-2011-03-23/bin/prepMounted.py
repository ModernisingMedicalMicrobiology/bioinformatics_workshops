#!/usr/bin/env python3
import sys
import pandas as pd
from argparse import ArgumentParser
from io import StringIO

def getSampleSheet(sheet):
    s,d='',{}
    with open(sheet,'rt') as input_sheet:
        for line in input_sheet:
            l=line.split(',')
            if len(l) > 1:
                d[l[0]]=l[-1].strip()
            if line.strip()=='[Data]':
                break
        for line in input_sheet:
            s+=line
    sheet=StringIO(s)
    df=pd.read_csv(sheet)
    for i in d:
        df[i]=d[i]
    return df

def genInputs(df):
    headers=['index','subindex','sample_name','sample_filename','sample_file_extension',
            'sample_host','sample_collection_date','sample_country','submission_title','submission_description','submitter_organisation','submitter_email','instrument_platform','instrument_model','instrument_flowcell']

    df['sample_name']=df['Sample_ID']
    df['sample_file_extension']='fastq.gz'
    df['sample_host']='Homo sapiens'
    df['sample_collection_date']='2021-01-01'
    df['sample_country']='United Kingdom'
    df['submission_title']=df['Experiment Name']
    df['submission_description']=df['Description']
    df['submitter_organisation']='University of Oxford'
    df['submitter_email']='nicholas.sanderson@ndm.ox.ac.uk'
    df['instrument_platform']='Illumina pair-ended sequencing'
    df['instrument_model']='Illumina ' +  df['Instrument Type']
    df['instrument_flowcell']=len(df)
    df['index']=df.index+1
    df=pd.concat([df,df])
    df['subindex']=df.groupby('Sample_ID').transform('cumcount')
    df['subindex']=df['subindex']+1
    df['sample_filename']=df['folder'] + '/' + df['Sample_Name'] + '/r' + df['subindex'].map(str) + '.fq.gz'
    df.sort_values(by=['index','subindex'],inplace=True)
    return df[headers]

def run(opts):
    samplesheet='{0}/SampleSheet.csv'.format(opts.folder) 
    df=getSampleSheet(samplesheet)
    df['folder']=opts.folder
    df=genInputs(df)
    df.to_csv(opts.output,index=False)

if __name__ == "__main__":
    # args
    parser = ArgumentParser(description='create inputs.csv from SampleSheet.csv')
    parser.add_argument('-o', '--output', required=False,default='inputs.csv',
                    help='output file, default=inputs.csv')
    parser.add_argument('-f', '--folder', required=True,
                    help='folder containing run')
    opts, unknown_args = parser.parse_known_args()
    run(opts)

