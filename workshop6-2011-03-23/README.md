# Uploading Illumina runs from computer science (analysis1 etc.) to SP3 using catsup

This is just a demonstration of the commands I have been using. I have written a cheat script to prepare the inputs.csv file from the samplesheet.csv in the illumina run directory.

We'll be using `catsup.py` that Fan and Denis have written for SP3 uploads, for more information, see their github page here https://github.com/oxfordmmm/catsup

## Login to the server

We're using the `mmm-slurm20h` ubuntu 20.04 server that Jez made, because we need at least python 3.6, but first you have to ssh into `analysys1`. 

```shell
ssh nick@ndm.local@analysis1.mmmoxford.uk -p 8081 

ssh 192.168.7.170
```

## clone a copy of catsup to somewhere you keep your software

Please visit the catsup github page where there are detailed instructions on how to install catsup and its dependencies. 

I use this config in the directory I'm uploading from:
```json
{
    "number_of_example_samples": 4,
    "number_of_files_per_sample": 2,
    "pipeline": "catsup-kraken2",
    "nextflow_additional_params": "-process.executor slurm",
    "pipelines":
    {
	"catsup-kraken2":
	{
	    "script": "/home/ndm.local/nick/soft/catsup/pipelines/catsup-kraken2/catsup-kraken2.nf",
	    "image": "/home/ndm.local/nick/soft/catsup/fatos-20200320T140813_2.2.img",
	    "human_ref": "/home/ndm.local/nick/dbs/kraken/kraken/minikraken2_v2_8GB_201904_UPDATE/"
	}

    },
    "upload":
    {
	"s3":
	{
	    "bucket": "s3://catsup",
	    "s3cmd-config": "/home/ndm.local/nick/.s3cfg-catsup"
	}
    }
}
```

And this is my `~/.s3cfg-catsup` with the access key removed

```
[default]
access_key = 
access_token =
add_encoding_exts =
add_headers =
bucket_location = UK
ca_certs_file =
cache_file =
check_ssl_certificate = True
check_ssl_hostname = True
cloudfront_host = cloudfront.amazonaws.com
content_disposition =
content_type =
default_mime_type = binary/octet-stream
delay_updates = False
delete_after = False
delete_after_fetch = False
delete_removed = False
dry_run = False
enable_multipart = True
encoding = UTF-8
encrypt = False
expiry_date =
expiry_days =
expiry_prefix =
follow_symlinks = False
force = False
get_continue = False
gpg_command = /usr/bin/gpg
gpg_decrypt = %(gpg_command)s -d --verbose --no-use-agent --batch --yes --passphrase-fd %(passphrase_fd)s -o %(output_file)s %(input_file)s
gpg_encrypt = %(gpg_command)s -c --verbose --no-use-agent --batch --yes --passphrase-fd %(passphrase_fd)s -o %(output_file)s %(input_file)s
gpg_passphrase =
guess_mime_type = True
host_base = test-s3.mmmoxford.uk
host_bucket = s3.bmrc.ac.uk
human_readable_sizes = False
invalidate_default_index_on_cf = False
invalidate_default_index_root_on_cf = True
invalidate_on_cf = False
kms_key =
limit = -1
limitrate = 0
list_md5 = False
log_target_prefix =
long_listing = False
max_delete = -1
mime_type =
multipart_chunk_size_mb = 15
multipart_max_chunks = 10000
preserve_attrs = True
progress_meter = True
proxy_host =
proxy_port = 0
put_continue = False
recursive = False
recv_chunk = 65536
reduced_redundancy = False
requester_pays = False
restore_days = 1
restore_priority = Standard
secret_key = asdftest123
send_chunk = 65536
server_side_encryption = False
signature_v2 = False
signurl_use_https = False
simpledb_host = sdb.amazonaws.com
skip_existing = False
socket_timeout = 300
stats = False
stop_on_error = False
storage_class =
throttle_max = 100
upload_id =
urlencoding_mode = normal
use_http_expect = False
use_https = True
use_mime_magic = True
verbosity = WARNING
website_endpoint = http://%(bucket)s.s3-website-%(location)s.amazonaws.com/
website_error =
website_index = index.html
```

## Identify the run to upload

The Illumina runs are found here, `/mnt/oxmountpoint/Kracken/filter_output/`

The Sample sheets contain information on the run name, samples, user etc. We want to upload Gillian's run `OX2801_Mtub`

```shell
grep 'Experiment Name,OX2801_Mtub' /mnt/oxmountpoint/Kracken/filter_output/*/SampleSheet.csv

/mnt/oxmountpoint/Kracken/filter_output/210114_M01746_0014_000000000-G5B4J/SampleSheet.csv:Experiment Name,OX2801_Mtub-MGITs-Oxford
```

The folder to use is `210114_M01746_0014_000000000-G5B4J`.

## Follow the steps from Catsup, using this script to make the input.csv file

There are four steps to the catsup upload, all using the same command. I've added in mystep to autogenerate the inputs.csv file. The script can and should be edited to meet the needs of other run types (miniseq, other species etc).

```shell
# step 1
python3 ~/soft/catsup/catsup.py 210114_M01746_0014_000000000-G5B4J

# Nick's step generate inputs from Samplesheet.csv
python3 prepMounted.py -f /mnt/oxmountpoint/Kracken/filter_output/210114_M01746_0014_000000000-G5B4J/ -o 210114_M01746_0014_000000000-G5B4J/inputs.csv

# step 2
python3 ~/soft/catsup/catsup.py 210114_M01746_0014_000000000-G5B4J

# step 3
python3 ~/soft/catsup/catsup.py 210114_M01746_0014_000000000-G5B4J

# step 4 
python3 ~/soft/catsup/catsup.py 210114_M01746_0014_000000000-G5B4J
```

## Go to SP3 to fetch and run the new data

Go to https://sp3ebi.mmmoxford.uk/ where the data will need to be fetched from:

`Datasets > New Fetch > Local server > <submission_uuid4>`

Where `submission_uuid4` can be found from the `sp3data.csv` file. Use the `copy` option for the `Fetch method`.

## LINK TO VIDEO OF WORKSHOP 
https://web.microsoftstream.com/video/08d24b2a-5af4-4f6e-adc2-ed1875055145
