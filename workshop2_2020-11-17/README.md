# Execution environments and source control

## Summary

* Running Enviornments
	* How am I going to run things?
	* How do make it easy to run different things?
	* How can other replicate how I run things?
* Source Control
	* How do I save my code?
	* How do I organise my development?
	* How do I share and/or work with others easily?

## Prerequisites 

This workshop assume you have access to the following. 

* A Linux (Ubuntu) or Mac machine
	* Can be a "server" only edition, i.e. terminal access only
  * Can be a remote machine (as long as the Software below is installed)
  * Other flavours of Linux should be braodly similar. If Debian based very few changes should be needed is installing through APT, if Red Hat you will have to find equivilant packages in YUM
* Software - available through APT (`sudo apt install`) or brew  on Mac (https://brew.sh/, `brew install`)
	* Python3.X
	* pip (python-pip or python3-pip)
  * git
  * (optionally) tig
  * Install with `sudo apt install python3 python3-pip git tig`
* Admin (`sudo`) access may be needed for other parts throughout

## Running Environments

This workshop will introduce a few different ways in which software can be run.

* virtualenv
* (ana)conda
* Containers (docker & singularity)

### virtualenv 

If you are running python software then pip is a good way of being able to install a variety of generally used modules. A pip package can install though a `pip install <PACKAGE>` or `pip3 install <PACKAGE>` command. 

However, pip can cause some problems. Firstly, it can be install packages in different ways (user only, sudo vs not sudo), which isn't always clear when on a system someone else has setup. Secondly, you can encounter conflicts between different programs dependencies. E.g. one piece of software may need a fairly recent version of a pip package, whilst another breaks when using such a version.

One way to solve such problems is to generate a unique environment for each piece of software you wish to run through `virtualenv`. Additionally this also makes documenting what dependencies your software has easier.

You install virtualenv through pip. N.B. You may get warnings about upgrading pip throughout, don't worry about those for now.

`pip install virtualenv`

When in an appropriate directory, you can create a blank environment using `virtualenv <NAME>`. However, you may also want to specify a specific version python to use:

`virtualenv -p /usr/bin/python3 venv_example`

After this has completed, you will have to activate the new environment. You'll see that this was successful when `(<NAME>)` precedes your normal prompt.

```
jez@MacBook-Pro workshop2_2020-11-17 % source venv_example/bin/activate
(venv_example) jez@MacBook-Pro workshop2_2020-11-17 %
```

This environment should only contain the bare essentials for python and pip to function. You can now install pip packages as you would normally.

```
(venv_example) jez@MacBook-Pro workshop2_2020-11-17 % pip list
Package    Version
---------- -------
pip        20.1.1
setuptools 46.4.0
wheel      0.34.2

(venv_example) jez@MacBook-Pro workshop2_2020-11-17 % pip install pyyaml
Processing /Users/jez/Library/Caches/pip/wheels/5e/03/1e/e1e954795d6f35dfc7b637fe2277bff021303bd9570ecea653/PyYAML-5.3.1-cp37-cp37m-macosx_10_14_x86_64.whl
Installing collected packages: pyyaml
Successfully installed pyyaml-5.3.1

(venv_example) jez@MacBook-Pro workshop2_2020-11-17 % pip list
Package    Version
---------- -------
pip        20.1.1
PyYAML     5.3.1
setuptools 46.4.0
wheel      0.34.2
(venv_example) jez@MacBook-Pro workshop2_2020-11-17 % python test_pyyaml.py 
test_pyyaml.py:8: YAMLLoadWarning: calling yaml.load() without Loader=... is deprecated, as the default Loader is unsafe. Please read https://msg.pyyaml.org/load for full details.
  print(yaml.dump(yaml.load(document)))
a: 1
b:
  c: 3
  d: 4
```

Once you have an environment you're happy with, you can export it for others use with `pip freeze` redirected towards an appropriate file. Standard practice is to create a requirement.txt file within the repo for this (`pip freeze > requirements.txt`). This can be used to replicate the environment wihtin a new virtualenv by using `pip install -r requirements.txt`.

You can exit the environment using `deactivate`. This will mean you can no longer access the packages you previously installed.

```
(venv_example) jez@MacBook-Pro workshop2_2020-11-17 % deactivate 
jez@MacBook-Pro workshop2_2020-11-17 % python test_pyyaml.py
Traceback (most recent call last):
  File "test_pyyaml.py", line 1, in <module>
    import yaml
ImportError: No module named yaml
```

If you wish to remove the enviroment it's as simple as deleting the generated directory

```
jez@MacBook-Pro workshop2_2020-11-17 % ls
Dockerfile      README.md       images          test_pyyaml.py  venv_example
jez@MacBook-Pro workshop2_2020-11-17 % rm -rf venv_example/
```

Thanks to https://docs.python-guide.org/dev/virtualenvs/ 

### (ana)conda

I'm not going to go into detail about this as a) it's conceptuatlly similar to virtualenv and b) I honestly haven't used it!

One notable difference however, is that it can be used for more than just python packages. It allows software in a variety of languages to be installed whilst maintaing the ability to change environments like with virtualenv.

### Containers

Containers work at a different level of abstraction to the environments described above. They are somewhere between these and a virtual machine (VM) in that abstract system level configuration in a similar way to packages and software were managed above. This means that things like system packages, file structure and permissions can be defined within them.

There are two primary examples we will consider here, Docker and Singularity, as they work fairly similarily. However other methods are available that extend upon these (e.g. Kubernetes) or are more heavily integrated into the host OS kernel (e.g. lxc/ lxd). 

The use of containers is how I would recommend packaging running environments for use on Nextflow (https://www.nextflow.io/)

N.B. I have provided scripts to help install docker and singularity on Ubuntu 20.04 machines, to use run `./install-docker-ubuntu.sh` and `./install-sing-ubuntu.sh`.

#### Docker 

Installing docker requires admin permissions on a machine and can be done through following the instructions for your device at https://docs.docker.com/engine/install/. N.B. if installing on Linux you may wish to perform some of the steps at https://docs.docker.com/engine/install/linux-postinstall/ (allow running without sudo). 

After install, a container can be run generally through `docker run <CONTAINER>`. However, this relies on the container having been definied in a way that runs something by default. In this scenario lets start with running a blank ubuntu container that we play around with

```
jez@MacBook-Pro ~ % docker run -it ubuntu /bin/bash
root@97fdb041f349:/# ls
bin   dev  home  lib32  libx32  mnt  proc  run   srv  tmp  var
boot  etc  lib   lib64  media   opt  root  sbin  sys  usr
 ```

A few of things to note here:

* By default you are provided root permission in the container. I should mention that there are scenarios the container can be provided root level access to the host (see https://docs.docker.com/engine/security/#docker-daemon-attack-surface)
* By default you are placed at the root of the file system. Also, the files here will not be the same as the the host
* When running interactively, any changes are deleted when exiting

Let's setup our own container so we can run use the pyyaml module we used in the vitualenv section. To do this we need to use a Dockerfile like that in this repo. Let's go through what's happening:

`from ubuntu:20.04`

This is defining what image to base ours of. This can use any of the images from DockerHub (https://hub.docker.com/)

```
RUN apt update && apt install -y python3 python3-pip && \
    rm -rf /var/lib/apt/lists/*

RUN pip3 install pyyaml
```

The `RUN` command allows any commands that would be run from the command to be used. In this case we're installing some APT packages and pip modules and should look similar to how you would install on your own machine. The last part of the apt command is to tidy up some of the package information to keep the image fairly small.

It's important to try and organise commands in a logical manner as each command will make another 'layer' in the image. When you remake an image, docker will try to re-use parts of the image that haven't changed, with later commands being dependent on earlier steps not having changed. Therefore, try to put the most changable parts of the image at the end.

`COPY test_pyyaml.py /`

The `COPY` command allows you to bring in files from the build location into the image.

`ENTRYPOINT ["python3", "test_pyyaml.py"]`

The `ENTRYPOINT` command will define what will be run by the container by default. If you know the container will be used for a specific purpose this can be automate the process. Otherwise you may not want this.

To make the image we need to use the command below. Note that the jezsw part here refers to a user or group to whom the image belongs.

`docker build -f Dockerfile -t jezsw/workshop2_pyyaml .`

This will allow us to then run it as follows

```
jez@MacBook-Pro workshop2_2020-11-17 % docker run jezsw/workshop2_pyyaml           
test_pyyaml.py:8: YAMLLoadWarning: calling yaml.load() without Loader=... is deprecated, as the default Loader is unsafe. Please read https://msg.pyyaml.org/load for full details.
  print(yaml.dump(yaml.load(document)))
a: 1
b:
  c: 3
  d: 4
```

If logged into dockerhub, you can also push the image to your account for others to use:

```
jez@MacBook-Pro workshop2_2020-11-17 % docker push jezsw/workshop2_pyyaml 
The push refers to repository [docker.io/jezsw/workshop2_pyyaml]
fb039c019b0e: Pushed 
04871f455ddc: Pushed 
3bece65971b8: Pushed 
cc9d18e90faa: Mounted from library/ubuntu 
0c2689e3f920: Mounted from library/ubuntu 
47dde53750b4: Mounted from library/ubuntu 
latest: digest: sha256:cb042124695773f5e928efb32d529455ec85e41e8873ff71fa3091287d29844b size: 1572
```

This will be made public by default and can therefore be run directly as above or pulled to be run locally later with `docker pull jezsw/workshop2_pyyaml`.

### Singularity

Singularity is conceptually largely the same as docker and can easily convert images built for docker to be used. The main advantages are:

* Containers are run as a specific user rather than root.
* Parts of the host file system can be more easily mounted into the container if outputs need to be shared.

Admin permissions are needed to initially install singularity, following the instructions at https://sylabs.io/guides/3.6/user-guide/quick_start.html#quick-installation-steps. Following this we could run the image with 

`singularity run docker://jezsw/workshop2_pyyaml`

N.B. Because binding file systems into the container is more common in singularity it may try running in the working directory its run from. Because of this, it may easier to run through `singularity shell <CONTAINER>` which gives you access to an interactive container or `singularity exec <CONTAINER <COMMAND>` which allows you to specific a specific command to run (Even when an `ENTRYPOINT` has been defined).

## Source Control

### Basics

This will breifly go through the main steps of version control

* Clone/ init
* Branch
* Stage
* Commit
* Merge
* Tags

A good website to test what effect different git commands may have is https://learngitbranching.js.org/?NODEMO.

#### Clone/ init

There are three main ways you'll start doing version control:

1. In a local directory.
2. Creating a new repo on gitlab etc.
3. Using an exisiting repo.

This example will assume 2, mainly so that things like tracking the remote repo are automatically setup! The mechanics of 2 and 3 are fairly similar, with the main difference being political. This will be discussed somewhat later but is a whole subject in it's own right.

If you wish to make a new repo on GitLab, first click the New Project button near the top of the home page when logged in. 

![Gitlab Homepage](images/gitlab_home.png)

Most of the next page is self explanitory, but the choice of user or group for the URL and visibility level chosen affects who will be able to view the project. This could be limited just to you, based on the membership of the group or freely to the public.

![Gitlab New Project](images/gitlab_new.png)

When you set this up you setup the space on your local machine with a `clone` command. Usually when doing this you'll have the choice of doing this through either SSH or HTTPS.

`git clone git@gitlab.com:ModernisingMedicalMicrobiology/bioinformatics_workshops.git`

N.B. Here's as good a place as any to discuss naming conventions. Programmers hate spaces, they tend to be poorly accounted for so are avoided wherever possible. Instead you'll see the following ways of splitting words:

1. Underscores or snakecase (e.g. bioinformatics_workshop)
2. CamelCase (e.g. bioinformaticsWorkshop)

Dependent on area there may be guidelines or rules on what to use.  Generally I'd suggest be consistent in your own work, but adapt to be consistent to a standard that has already been set in other work.

### Branch

Before adding to or editing the files in the repo, it's good practice to make a branch in which to do it. All repos begin with a master branch, this is the main flow of work that everything else breaks off from.

Other branches should generally be used to implement a specific feature or fix. The scope of these can vary widely, but the overall aim should be that when completed it can integrated back into the master to produce a working version. This doesn't mean that it's final version or that features won't be missing, but someone should be able to clone it and have it work.

To create a new branch off the exisiting branch (in this case the master):

`git checkout -b workshop2`

### Stage

After you've done some work, you'll need to stage the changes you've made. This covers both new files you've created and files that have been changed.

As this is a fresh directory we can just add everything.

`git add *`

You may want to be more selective about what you're adding. Firstly, you can just add specific files, e.g. when you editing existing files

```
jez@MacBook-Pro workshop2_2020-11-17 % git status
On branch workshop2
Your branch is up to date with 'origin/workshop2'.

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
        modified:   README.md

jez@MacBook-Pro workshop2_2020-11-17 % git add README.md 
jez@MacBook-Pro workshop2_2020-11-17 % git status
On branch workshop2
Your branch is up to date with 'origin/workshop2'.

Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
        modified:   README.md
```

You may also wish to use a `.gitignore` file to stop git from looking for certain types of files. E.g. In this repo I don't want it to find the files for virtual environments or DS_Store.

### Commit

After you've staged the changes you want to make you need to commit them to the history. Anytime you do this, you should add a message to summarise what the changes were for.

```
jez@MacBook-Pro workshop2_2020-11-17 % git commit -m 'A meaningful example message'       
[workshop2 1e01b5d] A meaningful example message
 1 file changed, 7 insertions(+), 7 deletions(-)
```

### Push

The basic command to use here is `git push`. However, because the branch doesn't exist in the remote repo, we need to make to create it there too: 

```
jez@MacBook-Pro workshop2_2020-11-17 % git push --set-upstream origin workshop2
Enumerating objects: 9, done.
Counting objects: 100% (9/9), done.
Delta compression using up to 8 threads
Compressing objects: 100% (7/7), done.
Writing objects: 100% (8/8), 1.64 MiB | 19.97 MiB/s, done.
Total 8 (delta 1), reused 0 (delta 0), pack-reused 0
remote: 
remote: To create a merge request for workshop2, visit:
remote:   https://gitlab.com/ModernisingMedicalMicrobiology/bioinformatics_workshops/-/merge_requests/new?merge_request%5Bsource_branch%5D=workshop2
remote: 
To gitlab.com:ModernisingMedicalMicrobiology/bioinformatics_workshops.git
 * [new branch]      workshop2 -> workshop2
Branch 'workshop2' set up to track remote branch 'workshop2' from 'origin'.
```

You'll see the mention of `origin`. This is the default name that is given to the remote location and all of it's associsted branches. E.g. if you want to see all the branches in the remote location you can using the following:

```
jez@MacBook-Pro workshop2_2020-11-17 % git branch -a
  master
* workshop2
  remotes/origin/HEAD -> origin/master
  remotes/origin/master
  remotes/origin/notebook
  remotes/origin/workshop2
```

It also means that local history is managed seperately until the remote repo is checked. E.g. if you make subsequent changes and don't push you'll see something like this:

```
jez@MacBook-Pro workshop2_2020-11-17 % git status
On branch workshop2
Your branch is ahead of 'origin/workshop2' by 1 commit.
  (use "git push" to publish your local commits)
```

Or if using a simple branch visualisation using `tig -all` (obtainable with apt on Ubuntu or brew on Mac).

![Tig Example](images/tig_example.png)

### Fetch/ Pull

All of the above worked because we were working on the code in one place. But imagine you also had this code on another machine. After some of the above changes this location won't initially be aware of them!

![Out of date history](images/tig_old.png)

There's two commands that can be used to make it aware. The first is `fetch`. This simply updates the origin branches/ history but not the working files (known as the `HEAD`).

![Post fetch](images/tig_fetch.png)

To actually update the files to the most recent changes you use `pull`. This will automatically fetch beforehand, but sometimes you may wish to look at changes before applying them. 

```
jez@MacBook-Pro workshop2_2020-11-17 % git pull 
warning: Pulling without specifying how to reconcile divergent branches is
discouraged. You can squelch this message by running one of the following
commands sometime before your next pull:

  git config pull.rebase false  # merge (the default strategy)
  git config pull.rebase true   # rebase
  git config pull.ff only       # fast-forward only

You can replace "git config" with "git config --global" to set a default
preference for all repositories. You can also pass --rebase, --no-rebase,
or --ff-only on the command line to override the configured default per
invocation.

Updating b016f66..1e01b5d
Fast-forward
 workshop2_2020-11-17/README.md                    |  63 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++------
 workshop2_2020-11-17/images/vscode_extend.png     | Bin 0 -> 1262115 bytes
 workshop2_2020-11-17/{ => images}/vscode_git.png  | Bin
 workshop2_2020-11-17/{ => images}/vscode_venv.png | Bin
 4 files changed, 57 insertions(+), 6 deletions(-)
 create mode 100644 workshop2_2020-11-17/images/vscode_extend.png
```

This is also where conflicts can occur if the histories are different (hence the warning in the extra text), but I won't go into that here.

### Merge

When you're happy that the work on the branch is OK to be put into the master branch you'll need to do a merge. This can also used on other branches to update them with changes that have occurred elsewhere.

The example here will merge some changes in another branch to the one we've been working on. Firstly we'll fetch to update out history about the other branch.

```
jez@MacBook-Pro workshop2_2020-11-17 % git fetch --all
Fetching origin
remote: Enumerating objects: 10, done.
remote: Counting objects: 100% (10/10), done.
remote: Compressing objects: 100% (6/6), done.
remote: Total 6 (delta 3), reused 0 (delta 0), pack-reused 0
Unpacking objects: 100% (6/6), 1.35 MiB | 1.55 MiB/s, done.
From gitlab.com:ModernisingMedicalMicrobiology/bioinformatics_workshops
 * [new branch]      workshop2_anotherbranch -> origin/workshop2_anotherbranch
```

![Another branch in tig](images/tig_branch.png)

You may wish to see what hhas changed between two branch. This can be done by investigating individual commits in tig by moving the cursor and pressing enter.

![Diff in tig](images/tig_diff.png)

Or you can use the compare tools in GitLab

![Diff in GitLab](images/gitlab_diff.png)

You'll see that some the changes on both of these branches were on the same document (README.md). Where the files are readable (i.e. not binary) Git does a line by line comparision of these changes to see if they overlap. In this case because the changes are in different locations, they can be safely combined (after Git accounts for the line differences etc).

If the two histories changed the same places of a file, then attempting a merge will result in Git highlighting this as a conflict. These issues would need to be resolved with at least some assistance from the user. However, I will not get into this here!

Assuming no conflicts will occur, this is completed through a `git merge`. You may be asked to confirm a merge message.

```
jez@MacBook-Pro workshop2_2020-11-17 % git merge origin/workshop2_anotherbranch 
Auto-merging workshop2_2020-11-17/README.md
Merge made by the 'recursive' strategy.
 workshop2_2020-11-17/README.md                |   4 +++-
 workshop2_2020-11-17/images/vscode_remote.png | Bin 0 -> 1502617 bytes
 2 files changed, 3 insertions(+), 1 deletion(-)
 create mode 100644 workshop2_2020-11-17/images/vscode_remote.png
```

![Merge in tig](images/tig_merge.png)

Remember, these updates won't be saved on the remote repo unless you `git push` afterwards!

```
jez@MacBook-Pro workshop2_2020-11-17 % git push  
Enumerating objects: 13, done.
Counting objects: 100% (13/13), done.
Delta compression using up to 8 threads
Compressing objects: 100% (5/5), done.
Writing objects: 100% (5/5), 612 bytes | 612.00 KiB/s, done.
Total 5 (delta 3), reused 0 (delta 0), pack-reused 0
remote: 
remote: To create a merge request for workshop2, visit:
remote:   https://gitlab.com/ModernisingMedicalMicrobiology/bioinformatics_workshops/-/merge_requests/new?merge_request%5Bsource_branch%5D=workshop2
remote: 
To gitlab.com:ModernisingMedicalMicrobiology/bioinformatics_workshops.git
   450e4c8..c7a3925  workshop2 -> workshop2
```

After a branch has been merged, you probably should consider it's deletion if it is no longer needed. You can delete branches using `git branch -d <BRANCH_NAME>` or through the GitLab UI.

### Tags

Although you can always checkout a specific previous branch or commit to work from, sometimes you may wish to denote specific locations on the history with something more meaningful. This can be achieved through the use of tags.

You can create a tag on the current location you have checked out with `git tag -a <TAG_NAME>` or in a specific location with a command such as 

`git tag -a v0.1 6da1b9ed`

You may have to give a message as well. 

![Tag in tig](images/tig_tag.png)

These tags can be used specify versions or anything else the user neems appropriate. In case version numbering is of interest I'll describe the basics of semantic versioning, where a version number `X.Y.Z` is incremented as below:

```
X - Incompatable changes with previous versions
Y - Features/ functionality that is backwards compatable
Z - Bug fixes etc
```

This bring us nicely onto ....

### Best practices

* Branch early and often
	* A branch per "feature"
* Add unique code, not replicatable setup
  * Use requirements.txt, Dockerfiles etc
  * Include install instructions in README.md
* Commit with meaning
	* Represent a "meaningful" change
	* Use meaningful commit messages
* Master is sacred
* Specific deployments may want to use branches/ tags
	* Config files tracking?

## Integrated Development Environments (IDEs)

IDEs such as vscode provide an environment that can be customised to allow development of code or other files that someone needed virtual environments or version control will use. This can include 

* Simple sytax highlighting
* Auto-completition tools
* Automated testing setup and teardown
* Debugging aids
* Managment of virtual exexution enviroments (e.g. virtualenv and containers)
* Version control
* Development of code on remote machines 

![Using a virtual env in vscode](images/vscode_venv.png)

![Git in vscode](images/vscode_git.png)

![Extensions in vscode](images/vscode_extend.png)

![Remote in vscode](images/vscode_remote.png)
