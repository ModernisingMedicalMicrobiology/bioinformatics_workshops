#!/bin/bash

sudo apt update && sudo apt install -y \
    build-essential \
    libssl-dev \
    uuid-dev \
    libgpgme11-dev \
    squashfs-tools \
    libseccomp-dev \
    wget \
    pkg-config \
    git \
    cryptsetup \
    golang-go

export VERSION=3.6.4 && \
  wget https://github.com/sylabs/singularity/releases/download/v${VERSION}/singularity-${VERSION}.tar.gz && \
  tar -xzf singularity-${VERSION}.tar.gz
  
cd singularity && ./mconfig && \
  make -C builddir && \
  sudo make -C builddir install
